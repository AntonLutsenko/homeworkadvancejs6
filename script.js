const url = "https://api.ipify.org/?format=json"; //IP
const realAdress = "http://ip-api.com/json";

class Ip {
	constructor(userIp, country, regionName, city) {
		this.userIp = userIp;
		this.country = country;
		this.regionName = regionName;
		this.city = city;
	}

	showAdres() {
		document.body.insertAdjacentHTML(
			"beforeend",
			`
          <div class= "adress"></div>`
		);
		document.querySelector(".adress").insertAdjacentHTML(
			"beforeend",
			`     <p>Ваш IP: ${this.userIp}</p>
        <p>Ваша страна: ${this.country}</p>
        <p>Ваш регион: ${this.regionName}</p>
        <p>Ваш город: ${this.city}</p>`
		);
	}
}

async function getAdress() {
	const response = await fetch(url);
	const userIp = await response.json();
	const userDistrict = await fetch(`${realAdress}/${userIp.ip}`);
	const realDistrict = await userDistrict.json();
	const { country, regionName, city } = realDistrict;
	let ipInpho = new Ip(userIp.ip, country, regionName, city);
	ipInpho.showAdres();
}

document.querySelector(".btn").addEventListener("click", () => getAdress());
